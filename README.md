# gcloud-cli-setup

## You will need:
- cred.json file
	- in google cloud create `service account` and assign it correct project rights + download cred.json
- input `SPECIFY_GCLOUD_PROJECT` in `Dockerfile`


## Setup and use:
- `docker build -t gcloud:v1 .`
- `docker run -it --rm gcloud:v1 /bin/sh`
