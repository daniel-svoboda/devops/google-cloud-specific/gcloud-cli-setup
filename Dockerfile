FROM centos

COPY cred.json /root/.config/gcloud/cred.json
COPY . /tmp/

# gcloud auth:
ENV GOOGLE_APPLICATION_CREDENTIALS="/root/.config/gcloud/cred.json"
# terraform auth:
ENV GOOGLE_CLOUD_KEYFILE_JSON="/root/.config/gcloud/cred.json"

WORKDIR /tmp

RUN mkdir temp && cd temp && \
	yum install -y wget unzip && \
	# 
	# Kubectl setup
	wget https://storage.googleapis.com/kubernetes-release/release/v1.12.0/bin/linux/amd64/kubectl && \
	chmod +x kubectl && \
	mv kubectl /usr/local/bin/ && \
	#
	# GCE SDK repo setup
	echo -e """[google-cloud-sdk]\nname=Google Cloud SDK\nbaseurl=https://packages.cloud.google.com/yum/repos/cloud-sdk-el7-x86_64\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg\n\thttps://packages.cloud.google.com/yum/doc/rpm-package-key.gpg""" > /etc/yum.repos.d/google-cloud-sdk.repo && \
	#
	yum install -y google-cloud-sdk && \
	cd .. && rm -rf temp && \
	#
	gcloud auth activate-service-account --key-file=/root/.config/gcloud/cred.json && \
	gcloud config set project <SPECIFY_GCLOUD_PROJECT>


# to build image run from within terraform files directory:
	# docker build -t terraform_gce:v1 -f ../Dockerfile .
# or from where Dockerfile is:
# docker build -t terraform_infra:v1 .

# to run built image:
	# docker run -it --rm terraform_gce:v1 /bin/sh
